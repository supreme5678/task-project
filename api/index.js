import axios from "axios";
import { AsyncStorage, Platform } from "react-native";
//CAMT
// export const host = "10.65.13.234:8080";
// CON
export const host = "https://ta2k.mooo.com";

if (host === "" || host === undefined) {
  throw new Error("API is not assigned");
}

axios.interceptors.request.use(async (config) => {
  const token = await AsyncStorage.getItem("token");

  if (token != null) {
    config.headers = { Authorization: `Bearer ${token}` };
  }

  return config;
});

//------------------------*USER*------------------------

export function getSession() {
  return axios.get(`${host}/sessions`);
}

export function signin(email, password) {
  return axios.post(`${host}/signin`, {
    email,
    password,
  });
}

export function signup({
  firstname,
  lastname,
  email,
  password,
  profile_image,
}) {
  return axios.post(`${host}/signup`, {
    firstname,
    lastname,
    email,
    password,
    profile_image,
  });
}

//------------------------*TASK*------------------------

export function createTask(user_id, { taskName, tag, date, location, note }) {
  return axios.post(`${host}/user/${user_id}/task`, {
    taskName,
    tag,
    date,
    location,
    note,
  });
}

export function getAllTask(id) {
  return axios.get(`${host}/user/${id}/tasks`);
}

export function getOneTask(id) {
  return axios.get(`${host}/user/${id}/task`);
}

export function getTodayTask(id) {
  return axios.get(`${host}/user/${id}/today`);
}

export function addImage(userID, profile_image) {
  return axios.patch(`${host}/user/${userID}`, { profile_image });
}

export function updateInfo(userID, firstname, lastname) {
  return axios.patch(`${host}/user/${userID}/profile`, { firstname, lastname });
}

export function uploadImage(userID, form) {
  return axios.post(`${host}/images/${userID}/image`, form);
}

export function deleteTask(id) {
  return axios.delete(`${host}/task/${id}`);
}

export function uploadImageCloud(file) {
  const dataForm = new FormData();
  dataForm.append("file", file);
  dataForm.append("upload_preset", "task-preset-1");
  dataForm.append("cloud_name", "task-user-profile-image");
  console.log("dataForm", dataForm);
  return axios.post(
    `https://api.cloudinary.com/v1_1/task-user-profile-image/image/upload`,
    dataForm
  );
}
