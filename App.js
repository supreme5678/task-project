import React, { useState, useEffect } from "react";
import { Provider } from "react-redux";
import {
  Platform,
  StatusBar,
  StyleSheet,
  View,
  Image,
  Text,
} from "react-native";
import { AppLoading, SplashScreen } from "expo";
import { Asset } from "expo-asset";
import * as Font from "expo-font";
import { Ionicons } from "@expo/vector-icons";
import Colors from "./constants/Colors";
import AsyncStorage from "@react-native-community/async-storage";
import { persistStore, persistReducer } from "redux-persist";
import { createStore, applyMiddleware } from "redux";
import { PersistGate } from "redux-persist/integration/react";
import { rootReducer } from "./redux/index";
import MainNavitator from "./route/MainNavitator";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
};
const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReducer);
const persistor = persistStore(store);

export default function App(props) {
  const [isSplashReady, setIsSplashReady] = useState(false);
  const [isAppReady, setIsAppReady] = useState(false);

  const _cacheSplashResourcesAsync = async () => {
    const gif = require("./assets/images/splash.png");
    return Asset.fromModule(gif).downloadAsync();
  };
  const _cacheResourcesAsync = async () => {
    SplashScreen.hide();
    await Promise.all([
      Asset.loadAsync([
        //Images here
        require("./assets/images/splash.png"),
        require("./assets/images/logo.png"),
      ]),

      Font.loadAsync({
        // This is the font that we are using for our tab bar
        Roboto: require("native-base/Fonts/Roboto.ttf"),
        Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        ...Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free to
        // remove this if you are not using it in your app
        // "space-mono": require("./assets/fonts/SpaceMono-Regular.ttf"),
      }),
    ]);
    setTimeout(() => setIsAppReady(true), 5000);
  };

  if (!isSplashReady) {
    return (
      <AppLoading
        startAsync={_cacheSplashResourcesAsync}
        autoHideSplash={false}
        onFinish={() => setIsSplashReady(true)}
        onError={handleLoadingError}
      />
    );
  }

  if (!isAppReady) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: Colors.SECONDARY,
          marginTop: -54,
        }}
      >
        <StatusBar barStyle="light-content" />
        <Image
          source={require("./assets/images/logo.png")}
          resizeMode="contain"
          style={{ width: 200, height: 200 }}
          onLoad={_cacheResourcesAsync}
        />
        <Text style={{ color: Colors.WHITE, fontSize: 16, fontWeight: "bold" }}>
          LOADING...
        </Text>
      </View>
    );
  }

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <View style={styles.container}>
          {Platform.OS === "ios" ? (
            <StatusBar
              translucent
              barStyle="dark-content"
              backgroundColor={Colors.PRIMARY}
            />
          ) : (
            <StatusBar
              translucent
              barStyle="dark-content"
              backgroundColor={Colors.PRIMARY}
            />
          )}
          <MainNavitator />
        </View>
      </PersistGate>
    </Provider>
  );
}
function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.PRIMARY,
    // marginTop: Platform.OS === "android" ? 0 : 0,
  },
});
