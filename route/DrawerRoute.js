import React from "react";
import { StyleSheet, Text } from "react-native";
import TaskList from "../screens/TaskList";
import Calender from "../screens/Calendar";
import FriendList from "../screens/FriendList";
import Profile from "../screens/Profile";
import Setting from "../screens/Setting";
import { SimpleLineIcons } from "@expo/vector-icons";
import { createDrawerNavigator } from "@react-navigation/drawer";
import HomeStack from "./HomeStack";
import Colors from "../constants/Colors";
import ChatStack from "./ChatStack";
import { Button } from "native-base";
import { useSelector } from "react-redux";
import { toggleChat } from "../redux/actions/settingAction";
const Drawer = createDrawerNavigator();

const DrawerRoute = (props) => {
  const { Navigation } = props;
  const chat = useSelector((state) => state.settingReducer.chat);
  return (
    <Drawer.Navigator initialRouteName="Home">
      {/* <Drawer.Screen
        name="LOGO"
        component={TaskList}
        options={{
          drawerLabel: () => null,
          title: null,
          drawerIcon: () => <AntDesign name="home" size={24} color="black" />,
        }}
      /> */}

      <Drawer.Screen
        name="Home"
        component={HomeStack}
        options={{
          drawerLabel: () => <Text style={styles.drawerText}>HOME</Text>,
          title: "Home",
          drawerIcon: () => (
            <>
              <SimpleLineIcons
                name="home"
                size={24}
                color={Colors.DARK_PRIMARY}
                style={{ marginLeft: 24 }}
              />
            </>
          ),
        }}
      />
      <Drawer.Screen
        name="Calendar"
        component={Calender}
        options={{
          drawerLabel: () => <Text style={styles.drawerText}>CALENDAR</Text>,
          title: "Home",
          drawerIcon: () => (
            <>
              <SimpleLineIcons
                name="calendar"
                size={24}
                color={Colors.DARK_PRIMARY}
                style={{ marginLeft: 24 }}
              />
            </>
          ),
        }}
      />
      {chat ? (
        <Drawer.Screen
          name="Friend"
          component={ChatStack}
          options={{
            drawerLabel: () => <Text style={styles.drawerText}>FRIEND</Text>,
            title: "Home",
            drawerIcon: () => (
              <>
                <SimpleLineIcons
                  name="bubbles"
                  size={24}
                  color={Colors.DARK_PRIMARY}
                  style={{ marginLeft: 24 }}
                />
              </>
            ),
          }}
        />
      ) : null}

      <Drawer.Screen
        name="Profile"
        component={Profile}
        options={{
          drawerLabel: () => <Text style={styles.drawerText}>PROFILE</Text>,
          title: "Home",
          drawerIcon: () => (
            <>
              <SimpleLineIcons
                name="user"
                size={24}
                color={Colors.DARK_PRIMARY}
                style={{ marginLeft: 24 }}
              />
            </>
          ),
        }}
      />
      <Drawer.Screen
        name="Setting"
        component={Setting}
        options={{
          drawerLabel: () => <Text style={styles.drawerText}>SETTING</Text>,
          title: "Home",
          drawerIcon: () => (
            <>
              <SimpleLineIcons
                name="settings"
                size={24}
                color={Colors.DARK_PRIMARY}
                style={{ marginLeft: 24 }}
              />
            </>
          ),
        }}
      />
    </Drawer.Navigator>
  );
};

export default DrawerRoute;

const styles = StyleSheet.create({
  drawerText: {
    marginLeft: 12,
    color: Colors.DARK_PRIMARY,
  },
});
