import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import TaskList from "../screens/TaskList";
import Welcome from "../screens/Welcome";
import CreateTask from "../screens/CreateTask";
import Colors from "../constants/Colors";
import { FontAwesome } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
const Stack = createStackNavigator();

const MainStack = (props) => {
  const { navigation } = props;
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{
          title: "My task",
          headerStyle: {
            backgroundColor: Colors.DARK_PRIMARY,
          },
          headerTintColor: "#000",
          headerTitleStyle: {
            fontWeight: "bold",
          },
          headerRight: () =>
            null,
            // <FontAwesome
            //   name="search"
            //   size={24}
            //   color="black"
            //   onPress={() => alert("This is a button!")}
            //   style={styles.searchIcon}
            // />
          headerLeft: () => (
            <Feather
              name="more-horizontal"
              size={24}
              color="black"
              onPress={() => navigation.openDrawer()}
              style={styles.moreIcon}
            />
          ),
        }}
        name="Task"
        component={TaskList}
      />
      {/* <Stack.Screen
        options={{ headerShown: false }}
        name="Welcome"
        component={Welcome}
      /> */}
      <Stack.Screen
        name="Create"
        component={CreateTask}
        options={{
          title: "Create task",
          headerStyle: {
            backgroundColor: Colors.DARK_PRIMARY,
          },
          headerTintColor: "#000",
          headerTitleStyle: {
            fontWeight: "bold",
          },

          headerLeft: () => (
            <Ionicons
              name="ios-arrow-back"
              size={24}
              color="black"
              style={styles.moreIcon}
              onPress={() => navigation.goBack()}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default MainStack;

const styles = StyleSheet.create({
  searchIcon: {
    marginRight: 16,
  },
  moreIcon: {
    marginLeft: 16,
  },
});
