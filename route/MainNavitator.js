import React from "react";
import { StyleSheet, Text, View } from "react-native";
// import LinkingConfiguration from "./navigation/LinkingConfiguration";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import ProfileStack from "./ProfileStack";
import DrawerRoute from "./DrawerRoute";
import { useSelector } from "react-redux";

const Stack = createStackNavigator();

export default function MainNavitator() {
  const token = useSelector((state) => state.authReducer.token);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {token === undefined ? (
          <Stack.Screen
            name="FirstPart"
            component={ProfileStack}
            options={{
              title: "My task",
              header: () => null,
            }}
          />
        ) : (
          <Stack.Screen
            name="SecondPart"
            component={DrawerRoute}
            options={{
              header: () => null,
            }}
          />
        )}
      </Stack.Navigator>
      {/* <DrawerRoute /> */}
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({});
