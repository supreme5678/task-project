import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import Colors from "../constants/Colors";
import { FontAwesome } from "@expo/vector-icons";
import { Feather } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import ChatChannel from "../screens/ChatChannel";
import FriendList from "../screens/FriendList";

const Stack = createStackNavigator();

const ChatStack = (props) => {
  const { navigation } = props;
  return (
    <Stack.Navigator>
      <Stack.Screen
        options={{
          title: "FriendList",
          headerStyle: {
            backgroundColor: Colors.DARK_PRIMARY,
          },
          headerTintColor: "#000",
          headerTitleStyle: {
            fontWeight: "bold",
          },
          headerRight: () => (
            <FontAwesome
              name="search"
              size={24}
              color="black"
              onPress={() => alert("This is a button!")}
              style={styles.searchIcon}
            />
          ),
          headerLeft: () => (
            <Feather
              name="more-horizontal"
              size={24}
              color="black"
              onPress={() => navigation.openDrawer()}
              style={styles.moreIcon}
            />
          ),
        }}
        name="FriendList"
        component={FriendList}
      />

      <Stack.Screen
        name="Chat"
        component={ChatChannel}
        options={{
          title: "Chat",
          headerStyle: {
            backgroundColor: Colors.DARK_PRIMARY,
          },
          headerTintColor: "#000",
          headerTitleStyle: {
            fontWeight: "bold",
          },

          headerLeft: () => (
            <Ionicons
              name="ios-arrow-back"
              size={24}
              color="black"
              style={styles.moreIcon}
              onPress={() => navigation.navigate("FriendList")}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default ChatStack;

const styles = StyleSheet.create({
  searchIcon: {
    marginRight: 16,
  },
  moreIcon: {
    marginLeft: 16,
  },
});
