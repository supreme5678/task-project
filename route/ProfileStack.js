import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "../screens/Login";
import Register from "../screens/Register";
import RegisterImg from "../screens/RegisterImg";
import Welcome from "../screens/Welcome";

const Stack = createStackNavigator();

const ProfileStack = (props) => {
  const { navigation } = props;
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={Login}
        options={{
          title: "My task",
          header: () => null,
        }}
      />

      <Stack.Screen
        name="Register"
        component={Register}
        options={{
          header: () => null,
        }}
      />

      <Stack.Screen
        name="RegisterImg"
        component={RegisterImg}
        options={{
          header: () => null,
        }}
      />
      <Stack.Screen
        name="Welcome"
        component={Welcome}
        options={{
          header: () => null,
        }}
      />
    </Stack.Navigator>
  );
};

export default ProfileStack;

const styles = StyleSheet.create({});
