import React, { useState } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Platform,
  Button,
  TouchableOpacity,
} from "react-native";
import {
  Form,
  Item,
  Input,
  Label,
  Picker,
  Thumbnail,
  Card,
  CardItem,
  Left,
  Body,
  Text,
} from "native-base";
import Colors from "../constants/Colors";
import AppButton from "../components/AppButton";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import { createTask, getSession } from "../api";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const CreateTask = (props) => {
  const { navigation } = props;
  const [tag, settag] = useState("Important");
  const [location, setlocation] = useState({
    locationName: "Chomdoi 2",
    lat: 18.803935,
    long: 98.960059,
  });
  const [date, setdate] = useState(new Date());
  const [note, setnote] = useState("");
  const [taskName, settaskName] = useState("");
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisible] = useState(false);
  const [loading, setloading] = useState(false);
  const onTaskNameChange = (event) => {
    settaskName(event.nativeEvent.text);
  };

  const onNoteChange = (event) => {
    setnote(event.nativeEvent.text);
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const showTimePicker = () => {
    setTimePickerVisible(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const hideTimePicker = () => {
    setTimePickerVisible(false);
  };

  const handleDateConfirm = (date) => {
    hideDatePicker();
    setdate(date);
    console.log("date", date);
  };

  const handleTimeConfirm = (time) => {
    hideTimePicker();
    setdate(time);
    console.log("time", time);
  };

  const onValueChange2 = (value) => {
    console.log("value", value);
    settag(value);
  };

  const onSetDate = (value) => {
    console.log("value", value);

    setchosenDate(value);
  };

  const handleTag = (value) => {
    switch (value) {
      case "Important":
        return "red";
      case "In progress":
        return "orange";
      case "Deadline":
        return "yellow";
      case "Work":
        return "purple";
      case "Family":
        return "lightgreen";
      case "Not Critical":
        return "lightblue";
      default:
        return "white";
    }
  };

  const onSaveTask = (event) => {
    setloading(true);
    getSession().then((res) => {
      createTask(res.data.ID, { taskName, tag, date, location, note })
        .then((res) => {
          console.log("res", res);
          setTimeout(() => {
            setloading(false);
            navigation.navigate("Task");
          }, 1000);
        })
        .catch((err) => {
          alert(err);
          setloading(false);
        });
    });
  };

  return (
    <ScrollView style={{ backgroundColor: Colors.PRIMARY }}>
      <View style={styles.container}>
        <View style={styles.header}>
          <Form>
            <Item
              style={{
                borderColor: Colors.TRANSPARENT,
                color: "white",
                paddingVertical: 2,
                backgroundColor: Colors.DARK_PRIMARY,
                borderRadius: 36,
                paddingHorizontal: 24,
              }}
            >
              {/* <Label style={{ color: Colors.WHITE }}>Task name</Label> */}
              <Input
                style={{
                  color: Colors.WHITE,
                  backgroundColor: "transparent",
                  fontWeight: "bold",
                }}
                placeholder="Task name"
                placeholderTextColor="white"
                value={taskName}
                onChange={onTaskNameChange.bind(this)}
              />
            </Item>
            <Item
              picker
              style={{
                borderColor: "transparent",
                // backgroundColor: Colors.WHITE,
                color: Colors.WHITE,
                marginTop: 12,
              }}
            >
              <Thumbnail
                small
                style={{
                  backgroundColor: handleTag(tag),
                  height: 16,
                  width: 48,
                  marginLeft: 24,
                  borderColor: Colors.WHITE,
                  borderWidth: 1,
                }}
              />
              <Picker
                mode="dialog"
                textStyle={{ color: Colors.WHITE }}
                iosIcon={
                  <MaterialCommunityIcons
                    name="tag-plus"
                    size={24}
                    color="white"
                  />
                }
                style={{
                  width: undefined,
                  color: "white",
                }}
                placeholder="Add Tag"
                // placeholderStyle={{ color: "#fff", backgroundColor: "black" }}
                // placeholderIconColor="#fff"
                selectedValue={tag}
                onValueChange={onValueChange2.bind(this)}
              >
                <Picker.Item
                  label="Important"
                  value="Important"
                  style={{ color: "white" }}
                />
                <Picker.Item label="In progress" value="In progress" />
                <Picker.Item label="Deadline" value="Deadline" />
                <Picker.Item label="Work" value="Work" />
                <Picker.Item label="Family" value="Family" />
                <Picker.Item label="Not Critical" value="Not Critical" />
              </Picker>
            </Item>
          </Form>
        </View>
        <View style={styles.listGroup}>
          {/* DATE */}
          <Card style={[styles.card]}>
            <CardItem style={styles.cardItem}>
              <Left>
                <Body>
                  <Text>Event Date</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem style={styles.bottom}>
              <Left>
                {/* <DatePicker
                  defaultDate={new Date(2020, 6, 8)}
                  minimumDate={new Date(2020, 1, 1)}
                  maximumDate={new Date(2030, 12, 31)}
                  locale={"en"}
                  timeZoneOffsetInMinutes={undefined}
                  animationType={"slide"}
                  androidMode={"default"}
                  placeHolderText="Select date"
                  textStyle={{ color: "green" }}
                  placeHolderTextStyle={{ color: "#d3d3d3" }}
                  onDateChange={onSetDate.bind(this)}
                  disabled={false}
                  modalTransparent={true}
                /> */}
                <View
                  style={{
                    flex: 1,
                    justifyContent: "flex-start",
                    alignItems: "center",
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      marginBottom: Platform.OS === "android" && 24,
                    }}
                  >
                    <Text note>Date :</Text>
                    {Platform.OS === "ios" ? (
                      <Button
                        color={Colors.DARK_PRIMARY}
                        onPress={showDatePicker}
                        title={date.toLocaleDateString("en-GB")}
                      />
                    ) : (
                      <TouchableOpacity onPress={showDatePicker}>
                        <Text style={{ color: Colors.DARK_PRIMARY }}>
                          {date.toLocaleDateString("en-GB")}
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                    }}
                  >
                    <Text note>Time :</Text>
                    {Platform.OS === "ios" ? (
                      <Button
                        color={Colors.DARK_PRIMARY}
                        onPress={showTimePicker}
                        title={date.toLocaleTimeString("en-GB")}
                      />
                    ) : (
                      <TouchableOpacity onPress={showTimePicker}>
                        <Text style={{ color: Colors.DARK_PRIMARY }}>
                          {date.toLocaleTimeString("en-GB")}
                        </Text>
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
                <DateTimePickerModal
                  isVisible={isDatePickerVisible}
                  mode={"date"}
                  onConfirm={handleDateConfirm}
                  onCancel={hideDatePicker}
                />
                <DateTimePickerModal
                  isVisible={isTimePickerVisible}
                  mode={"time"}
                  locale="en_GB"
                  onConfirm={handleTimeConfirm}
                  onCancel={hideTimePicker}
                  date={date}
                />
              </Left>
            </CardItem>
          </Card>
          {/* LOCATION */}
          {/* <Card style={[styles.card]}>
            <CardItem style={styles.cardItem}>
              <Left>
                <Body>
                  <Text>Location</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem style={styles.bottom}>
              <Left>
                <Text style={styles.boldTxt}>Date</Text>
              </Left>
            </CardItem>
          </Card> */}
          {/* NOTE */}
          <Card style={[styles.card]}>
            <CardItem style={styles.cardItem}>
              <Left>
                <Body>
                  <Text>Note</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem style={styles.bottom}>
              <Left>
                <Item rounded>
                  <Input
                    placeholder="Note something.."
                    value={note}
                    onChange={onNoteChange.bind(this)}
                  />
                </Item>
              </Left>
            </CardItem>
          </Card>
          <View style={{ marginTop: 24 }} />
          <AppButton
            loading={loading}
            title="SAVE"
            onPress={(event) => onSaveTask(event)}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default CreateTask;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
    alignItems: "center",
    // height: "100%",
  },
  header: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: Colors.PRIMARY,
    width: "100%",
    paddingHorizontal: 24,
    paddingVertical: 24,
  },
  listGroup: {
    flex: 6,
    paddingHorizontal: 24,
    width: "100%",
    backgroundColor: Colors.WHITE,
    // flexDirection: "row",
    alignItems: "center",
    paddingBottom: Platform.OS === "android" ? 24 : 160,
  },
  card: {
    borderRadius: 12,
    width: "100%",
    marginTop: 24,
  },
  cardItem: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.BLACK,
    backgroundColor: Colors.PRIMARY,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },
  //   top:
  bottom: {
    marginVertical: 8,
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12,
    backgroundColor: "rgba(255,255,255,0.2)",
  },
  thumb: {
    height: 12,
    width: 24,
    backgroundColor: "red",
    borderColor: Colors.WHITE,
    borderWidth: 1,
  },
  note: {
    color: Colors.WHITE,
  },
});
