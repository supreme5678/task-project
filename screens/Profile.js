import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Platform,
  StatusBar,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert,
} from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";
import Colors from "../constants/Colors";
import { Item, Label, Input, Form, Thumbnail, Badge } from "native-base";
import { getSession, updateInfo, uploadImageCloud, addImage } from "../api";
import AppButton from "../components/AppButton";
import * as ImagePicker from "expo-image-picker";
import { FontAwesome5 } from "@expo/vector-icons";
import Constants from "expo-constants";

const Profile = () => {
  const [firstname, setfirstname] = useState("");
  const [lastname, setlastname] = useState("");
  const [email, setemail] = useState("");

  const [showimg, setshowimg] = useState(
    "https://kyau.edu.bd/kyau/public/uploads/defult_image.png"
  );
  const [photo, setphoto] = useState(null);
  const [file, setfile] = useState(null);
  const [loading, setloading] = useState(false);

  const [loadingsave, setloadingsave] = useState(false);
  const [counter, setcounter] = useState(0);

  const [namefocus, setnamefocus] = useState(false);
  const [lastfocus, setlastfocus] = useState(false);

  const [imagefocus, setimagefocus] = useState(false);
  useEffect(() => {
    getSession().then((res) => {
      console.log("res", res.data);
      setfirstname(res.data.firstname);
      setlastname(res.data.lastname);
      setemail(res.data.email);
      setshowimg(res.data.profile_image);
    });
    setnamefocus(false);
    setlastfocus(false);
  }, [counter]);

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {
          status,
        } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
      if (Constants.platform.android) {
        const {
          status,
        } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
    setnamefocus(false);
    setlastfocus(false);
  }, []);

  const handleSave = () => {
    setloadingsave(true);
    getSession().then((res) => {
      updateInfo(res.data.ID, firstname, lastname)
        .then((upres) => {
          setTimeout(() => {
            setloadingsave(false);
            setcounter(counter + 1);
            setnamefocus(false);
            setlastfocus(false);
            Alert.alert("Saved change");
          }, 3000);

          console.log("upres", upres);
        })
        .catch((err) => {
          setloadingsave(false);
          alert(err);
        });
    });
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 0.5,
    });

    console.log(result);

    if (!result.cancelled) {
      console.log("result", result);
      setimagefocus(true);
      setshowimg(result.uri);
      setphoto(result);
    }
  };

  const cloudinaryUpload = () => {
    setloading(true);
    uploadImageCloud({
      name: Math.random().toString(36).substring(2, 15),
      type: photo.type,
      uri: photo.uri,
    })
      .then((res) => {
        console.log("data=====================>", res.data);
        // setfile(res.data.secure_url);
        getSession()
          .then((sesres) => {
            addImage(sesres.data.ID, res.data.secure_url)
              .then((addres) => {
                console.log("addres", addres);

                setTimeout(() => {
                  setloading(false);
                  setimagefocus(false);
                  Alert.alert("Saved change");
                }, 1000);
              })
              .catch((err) => {
                console.log("err", err);
                setloading(false);
                Alert.alert("An Error Occured While Set Image to Profile");
              });
          })
          .catch((err) => {
            console.log("err", err);
            setloading(false);
            Alert.alert("An Error Occured While Add Image to Profile");
          });
      })
      .catch((err) => {
        console.log("err", err);
        Alert.alert("An Error Occured While Uploading");

        setTimeout(() => {
          setloading(false);
        }, 1000);
      });
  };
  return (
    <>
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        style={{ backgroundColor: Colors.PRIMARY }}
      >
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.headerTxt}>Profile</Text>
          </View>
          <View style={styles.bottom}>
            <View style={styles.row2}>
              <TouchableOpacity onPress={pickImage}>
                <Badge
                  style={{
                    backgroundColor: "black",
                    position: "absolute",
                    alignSelf: "center",
                    zIndex: 2,
                    right: 0,
                    borderWidth: 1,
                    borderColor: "white",
                  }}
                >
                  <FontAwesome5 name="edit" size={16} color="white" />
                </Badge>
                <Image style={styles.image} source={{ uri: showimg }} />
              </TouchableOpacity>
              {imagefocus ? (
                <AppButton
                  title="UPLOAD"
                  loading={loading}
                  onPress={cloudinaryUpload}
                />
              ) : null}
            </View>
            <View style={styles.row}>
              <Form>
                <Item stackedLabel>
                  <Label>Name</Label>
                  <Input
                    value={firstname}
                    onChangeText={(tex) => setfirstname(tex)}
                    onFocus={() => setnamefocus(true)}
                  />
                </Item>
                <Item floatingLabel>
                  <Label>Last name</Label>
                  <Input
                    value={lastname}
                    onChangeText={(tex) => setlastname(tex)}
                    onFocus={() => setlastfocus(true)}
                  />
                </Item>
                <Item floatingLabel disabled>
                  <Label>E-mail</Label>
                  <Input disabled value={email} />
                </Item>
                <Item floatingLabel disabled>
                  <Label>Password</Label>
                  <Input disabled value="******" />
                </Item>
              </Form>
            </View>
            <View
              style={{ display: "flex", alignItems: "center", marginTop: 24 }}
            >
              {namefocus || lastfocus ? (
                <AppButton
                  title="SAVE"
                  onPress={handleSave}
                  loading={loadingsave}
                />
              ) : null}
            </View>
          </View>
        </View>
      </ScrollView>
    </>
  );
};

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Platform.OS === "android" ? getStatusBarHeight() : 0,
  },
  header: {
    flex: Platform.OS === "android" ? 1 : 1,
    justifyContent: "center",
    backgroundColor: Colors.PRIMARY,
    width: "100%",
    paddingHorizontal: 24,
    paddingVertical: Platform.OS === "android" ? 48 : 56,
  },
  bottom: {
    flex: 8,
    backgroundColor: Colors.WHITE,
    paddingBottom: 56,
    paddingHorizontal: 24,
  },
  headerTxt: {
    fontSize: 24,
    fontWeight: "bold",
    marginTop: Platform.OS === "ios" ? 24 : 0,
  },
  row: {
    flex: 1,
    // flexDirection: "row",
  },
  row2: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-evenly",
    paddingVertical: 24,
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 90,
    marginBottom: 24,
  },
});
