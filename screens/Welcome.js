import React from "react";
import { StyleSheet, View, Text, StatusBar, AsyncStorage } from "react-native";
import AppButton from "../components/AppButton";
import Colors from "../constants/Colors";
import { useDispatch } from "react-redux";
import { loginSuccess } from "../redux/actions/authAction";

const Welcome = (props) => {
  const { navigation } = props;
  const dispatch = useDispatch();
  const token = AsyncStorage.getItem("token");

  const onNext = () => {
    dispatch(loginSuccess(token));
  };
  return (
    <View style={styles.container}>
      <View style={styles.welcome}>
        <Text style={styles.boldText}>Welcome to</Text>
        <Text style={styles.boldText}>TASK</Text>
        <View style={{ marginBottom: 24 }} />
        <Text style={styles.quoteText}>
          Planing your task effectively and find special time to spend with your
          friend and family
        </Text>
      </View>
      <View style={styles.buttonGroup}>
        <AppButton title="GET START" onPress={onNext} />
      </View>
    </View>
  );
};

export default Welcome;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
    height: "100%",
    alignItems: "center",
  },
  welcome: {
    flex: 2,
    justifyContent: "center",
    backgroundColor: Colors.PRIMARY,
    width: "100%",
    paddingHorizontal: 24,
  },
  buttonGroup: {
    flex: 1,
    justifyContent: "center",
  },
  boldText: {
    color: Colors.WHITE,
    fontWeight: "bold",
    fontSize: 24,
  },
  quoteText: {
    color: Colors.WHITE,
    opacity: 0.5,
    fontSize: 20,
  },
});
