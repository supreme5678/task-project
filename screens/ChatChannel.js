import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { StreamChat } from "stream-chat";
import { Chat, Channel, MessageList, MessageInput } from "stream-chat-expo";

const ChatChannel = ({ route, navigation }) => {
  const { channel } = route.params;
  // console.log("channel", channel);
  // const channel = getChannel();
  const chatClient = new StreamChat("pu9sesn3x3de");
  const userToken =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoiY29vbC1kdXN0LTYifQ.GmNBVwVZWyKpipWALIF9jAZH-hun-5KwBEVgFAFCzf4";

  const user = {
    id: "still-sky-6",
    name: "Still sky",
    image:
      "https://stepupandlive.files.wordpress.com/2014/09/3d-animated-frog-image.jpg",
  };

  chatClient.setUser(user, userToken);

  return (
    <View style={styles.container}>
      <Chat client={chatClient}>
        <Channel channel={channel}>
          <View style={{ display: "flex", height: "100%" }}>
            <MessageList />
            <MessageInput />
          </View>
        </Channel>
      </Chat>
    </View>
  );
};

export default ChatChannel;

const styles = StyleSheet.create({
  container: {
    marginTop: getStatusBarHeight(),
    paddingBottom: 36,
  },
});

// function getChannel() {
//   chatClient.setUser(
//     {
//       id: user,
//     },
//     userToken
//   );

//   return chatClient.channel("messaging", "general", {
//     image:
//       "https://s3-us-west-2.amazonaws.com/s.cdpn.io/195612/chat_avatar_01_green.jpg",
//     name: "Talk about the documentation",
//   });
// }
