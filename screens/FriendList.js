import React, { createRef } from "react";
import { View, SafeAreaView, Text, TouchableOpacity } from "react-native";
import { StreamChat } from "stream-chat";
import {
  Avatar,
  Chat,
  Channel,
  MessageList,
  MessageInput,
  ChannelList,
  IconBadge,
} from "stream-chat-expo";
import truncate from "lodash/truncate";

const chatClient = new StreamChat("pu9sesn3x3de");

const CustomChannelPreview = (props) => {
  const channelPreviewButton = createRef();

  const userToken =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoicHVycGxlLW1vdW50YWluLTEifQ.BpKTup6XFv-2lXpiKNjB37vT9XCAociZPum1_gwUiIk";

  const user = {
    id: "purple-mountain-1",
    name: "Purple mountain",
    image:
      "https://stepupandlive.files.wordpress.com/2014/09/3d-animated-frog-image.jpg",
  };

  chatClient.setUser(user, userToken);

  const onSelectChannel = () => {
    props.setActiveChannel(props.channel);
  };

  const { channel } = props;
  const unreadCount = channel.countUnread();

  return (
    <TouchableOpacity
      style={{
        display: "flex",
        flexDirection: "row",
        borderBottomColor: "#EBEBEB",
        borderBottomWidth: 1,
        padding: 10,
      }}
      onPress={onSelectChannel}
    >
      <Avatar image={channel.data.image} size={40} />
      <View
        style={{
          display: "flex",
          flexDirection: "column",
          flex: 1,
          paddingLeft: 10,
        }}
      >
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Text
            style={{
              fontWeight: unreadCount > 0 ? "bold" : "normal",
              fontSize: 14,
              flex: 9,
            }}
            ellipsizeMode="tail"
            numberOfLines={1}
          >
            {channel.data.name}
          </Text>
          <IconBadge unread={unreadCount} showNumber>
            <Text
              style={{
                color: "#767676",
                fontSize: 11,
                flex: 3,
                textAlign: "right",
              }}
            >
              {props.latestMessage.created_at}
            </Text>
          </IconBadge>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const FriendList = (props) => {
  return (
    <SafeAreaView>
      <Chat client={chatClient}>
        <View style={{ display: "flex", height: "100%", padding: 10 }}>
          <ChannelList
            filters={{
              type: "messaging",
              members: { $in: ["purple-mountain-1"] },
            }}
            sort={{ last_message_at: -1 }}
            Preview={CustomChannelPreview}
            onSelect={(channel) => {
              props.navigation.navigate("Chat", {
                channel,
              });
            }}
          />
        </View>
      </Chat>
    </SafeAreaView>
  );
};

export default FriendList;
