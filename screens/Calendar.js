import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Button,
  Alert,
} from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { Calendar, CalendarList, Agenda } from "react-native-calendars";
import Colors from "../constants/Colors";
import { getSession, getAllTask, deleteTask } from "../api";
import { AppModal } from "../components/AppModal";
import Modal from "react-native-modal";
import { MaterialIcons } from "@expo/vector-icons";
const Calender = () => {
  const [items, setitems] = useState({});
  const [recieved, setrecieved] = useState([]);
  const important = {
    key: "Important",
    color: "red",
  };
  const inprogress = {
    key: "In progress",
    color: "orange",
  };
  const deadline = { key: "Deadline", color: "yellow" };
  const work = { key: "Work", color: "purple" };
  const family = { key: "Family", color: "lightgreen" };
  const notCritical = { key: "Not Critical", color: "lightblue" };
  const [mark, setmark] = useState({});
  const [loading, setloading] = useState(false);
  const [counter, setcounter] = useState(0);
  const [visible, setvisible] = useState(false);
  useEffect(() => {
    fetchData();
  }, [counter]);
  // console.log("recieved", recieved);

  const fetchData = () => {
    getSession().then((sesres) => {
      getAllTask(sesres.data.ID).then((res) => {
        setrecieved(res.data);
      });
    });
  };

  const handleDelete = (task_id) => {
    Alert.alert(
      "Are you sure?",
      "Delete this task",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => {
            deleteTask(task_id).then((res) => {
              fetchData();
              setTimeout(() => {
                loadItems();
              }, 3000);
              alert("Deleted");
            });
          },
        },
      ],
      { cancelable: false }
    );
  };
  const loadItems = (day) => {
    setloading(true);
    fetchData();
    setTimeout(() => {
      for (let i = -5; i < 5; i++) {
        const time = new Date().getTime() + i * 24 * 60 * 60 * 1000;
        const strTime = timeToString(time);
        items[strTime] = [];
      }
      for (let i = 0; i < recieved.length; i++) {
        const time = new Date(recieved[i].date).valueOf();
        const strTime = timeToString(time);
        items[strTime] = [];
        mark[strTime] = [];
      }

      Object.keys(items).filter((key) => {
        const temp = [];
        for (let j = 0; j < recieved.length; j++) {
          if (key === timeToString(new Date(recieved[j].date).valueOf())) {
            // temp.push(recieved[j]);
            // for (let k = 0; k < items.length; k++) {
            var tempDot = [];
            if ((recieved[j].tag = "Important")) {
              tempDot.push(important);
            } else if ((recieved[j].tag = "In progress")) {
              tempDot.push(inprogress);
            } else if ((recieved[j].tag = "Deadline")) {
              tempDot.push(deadline);
            } else if ((recieved[j].tag = "Work")) {
              tempDot.push(work);
            } else if ((recieved[j].tag = "Family")) {
              tempDot.push(family);
            } else if ((recieved[j].tag = "Not Critical")) {
              tempDot.push(notCritical);
            } else {
              tempDot.push(important);
            }

            mark[key].dots = tempDot;
            mark[key].selected = false;

            items[key].push({
              name: recieved[j].taskName,
              id: recieved[j].ID,
              tag: recieved[j].tag,
              note: recieved[j].note,
              date: recieved[j].date,
              ID: recieved[j].ID,
              height: 80,
            });
            // }
          }
        }
      });

      const newItems = {};
      Object.keys(items).forEach((key) => {
        newItems[key] = items[key];
      });

      const newMark = {};

      Object.keys(mark).forEach((key) => {
        newMark[key] = mark[key];
      });
      setmark(newMark);
      setloading(false);
    }, 5000);
  };

  // console.log("items--------------------", mark);

  const renderItem = (item) => {
    return (
      <TouchableOpacity
        style={[
          styles.item,
          { height: item.height, backgroundColor: Colors.PRIMARY },
        ]}
      >
        <Text style={{ fontWeight: "bold" }}>
          {item.name ? item.name : "...."}
        </Text>
        <Text>{new Date(item.date).toLocaleTimeString("en-GB")}</Text>
        <Text note>{item.note}</Text>
        <TouchableOpacity
          style={{ position: "absolute", top: 12, right: 12 }}
          onPress={() => handleDelete(item.ID)}
        >
          <MaterialIcons
            name="remove-circle-outline"
            size={24}
            color="#FE0000"
          />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };
  const toggleModal = () => {
    setvisible(!visible);
  };
  const onPressDay = (item) => {
    // setvisible(true);
    // alert("WOWSCS");
  };

  const handleTag = (value) => {
    switch (value) {
      case "Important":
        return "red";
      case "In progress":
        return "orange";
      case "Deadline":
        return "yellow";
      case "Work":
        return "purple";
      case "Family":
        return "lightgreen";
      case "Not Critical":
        return "lightblue";
      default:
        return Colors.PRIMARY;
    }
  };

  const renderEmptyDate = (day) => {
    return (
      <View style={styles.emptyDate}>
        <Text>This is free day !</Text>
      </View>
    );
  };

  const rowHasChanged = (r1, r2) => {
    return r1.name !== r2.name;
  };

  const timeToString = (time) => {
    const date = new Date(time);
    return date.toISOString().split("T")[0];
  };

  return (
    <View style={styles.container}>
      <Agenda
        items={items}
        onCalendarToggled={(calendarOpened) => {
          console.log(calendarOpened);
        }}
        // Callback that gets called on day press
        onDayPress={onPressDay.bind(this)}
        // Callback that gets called when day changes while scrolling agenda list
        onDayChange={(day) => {
          console.log("day changed");
        }}
        minDate={"2018-12-31"}
        maxDate={"2030-01-01"}
        // Max amount of months allowed to scroll to the past. Default = 50
        pastScrollRange={20}
        // Max amount of months allowed to scroll to the future. Default = 50
        futureScrollRange={20}
        loadItemsForMonth={loadItems.bind(this)}
        selected={new Date()}
        renderItem={renderItem.bind(this)}
        renderEmptyDate={renderEmptyDate.bind(this)}
        rowHasChanged={rowHasChanged.bind(this)}
        // If provided, a standard RefreshControl will be added for "Pull to Refresh" functionality. Make sure to also set the refreshing prop correctly.
        onRefresh={loadItems.bind(this)}
        // Set this true while waiting for new data from a refresh
        refreshing={loading}
        // Add a custom RefreshControl component, used to provide pull-to-refresh functionality for the ScrollView.
        refreshControl={null}
        // Agenda theme
        theme={{
          agendaDayTextColor: Colors.BLACK,
          agendaDayNumColor: Colors.BLACK,
          agendaTodayColor: Colors.DARK_PRIMARY,
          agendaKnobColor: Colors.DARK_PRIMARY,
          indicatorColor: Colors.DARK_PRIMARY,
          selectedDayTextColor: Colors.BLACK,
          selectedDayBackgroundColor: Colors.DARK_PRIMARY,
          dayTextColor: Colors.BLACK,
          textSectionTitleColor: Colors.BLACK,
          todayTextColor: Colors.DARK_PRIMARY,
          monthTextColor: Colors.DARK_PRIMARY,
          textDayFontWeight: "300",
          textMonthFontWeight: "bold",
          textDayHeaderFontWeight: "300",
          textDayFontSize: 16,
          textMonthFontSize: 16,
          textDayHeaderFontSize: 12,
        }}
        markingType={"multi-dot"}
        markedDates={mark}
        Agenda
        container
        style={{}}
      />
    </View>
  );
};

export default Calender;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: getStatusBarHeight(),
    backgroundColor: Colors.WHITE,
  },
  item: {
    // backgroundColor: ,
    flex: 1,
    borderRadius: 5,
    padding: 16,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 30,
    color: "#aaa",
  },
});
