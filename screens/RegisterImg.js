import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  AsyncStorage,
} from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";
import Colors from "../constants/Colors";
import * as ImagePicker from "expo-image-picker";
import { getSession, uploadImage, addImage, uploadImageCloud } from "../api";
import Constants from "expo-constants";
import { Spinner } from "native-base";
import axios from "axios";
// const uri = "https://kyau.edu.bd/kyau/public/uploads/defult_image.png";

export default function RegisterImg(props) {
  const { navigation } = props;
  const [showimg, setshowimg] = useState(
    "https://kyau.edu.bd/kyau/public/uploads/defult_image.png"
  );
  const [photo, setphoto] = useState(null);
  const [file, setfile] = useState(null);
  const [loading, setloading] = useState(false);

  const token = AsyncStorage.getItem("token");
  console.log("token", token);
  console.log("file", file);

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {
          status,
        } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
      if (Constants.platform.android) {
        const {
          status,
        } = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 0.5,
    });

    console.log(result);

    if (!result.cancelled) {
      console.log("result", result);
      setshowimg(result.uri);
      setphoto(result);
    }
  };

  //   const handleUpload = () => {
  //     getSession().then((res) => {
  //       const form = new FormData();
  //       form.append("photo", {
  //         image:
  //           Math.random().toString(36).substring(2, 15) +
  //           Math.random().toString(36).substring(2, 15),
  //         src: photo.uri,
  //         user_id: res.data.ID,
  //         type: photo.type,
  //       });
  //       console.log("form._parts", form);
  //       uploadImage(res.data.ID, form._parts[0]).then((res) => {
  //         console.log("res", res);
  //       });
  //     });
  //   };

  const cloudinaryUpload = () => {
    setloading(true);
    uploadImageCloud({
      name: Math.random().toString(36).substring(2, 15),
      type: photo.type,
      uri: photo.uri,
    })
      .then((res) => {
        console.log("data=====================>", res.data);
        setfile(res.data.secure_url);
        getSession()
          .then((sesres) => {
            addImage(sesres.data.ID, res.data.secure_url)
              .then((addres) => {
                console.log("addres", addres);

                setTimeout(() => {
                  setloading(false);
                  navigation.navigate("Welcome");
                }, 1000);
              })
              .catch((err) => {
                console.log("err", err);
                setloading(false);
                Alert.alert("An Error Occured While Set Image to Profile");
              });
          })
          .catch((err) => {
            console.log("err", err);
            setloading(false);
            Alert.alert("An Error Occured While Add Image to Profile");
          });
      })
      .catch((err) => {
        console.log("err", err);
        Alert.alert("An Error Occured While Uploading");

        setTimeout(() => {
          setloading(false);
        }, 1000);
      });
  };

  return (
    <>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="light-content"
      />
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        style={{ backgroundColor: Colors.SECONDARY }}
      >
        <View style={styles.container}>
          <Text style={{ paddingBottom: 80, color: Colors.WHITE }}>
            Final step, Add your profile image
          </Text>
          <View>
            <Image style={styles.image} source={{ uri: showimg }} />
            <TouchableOpacity style={styles.button} onPress={pickImage}>
              <Text style={styles.text}>Upload</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.bottom}>
          {photo === null ? (
            <TouchableOpacity
              style={{
                paddingVertical: 16,
                paddingHorizontal: 36,
                marginTop: 36,
              }}
              onPress={() => navigation.navigate("Welcome")}
            >
              <Text
                style={{
                  color: Colors.WHITE,
                  textAlign: "right",
                  textDecorationLine: "underline",
                }}
              >
                Skip
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity style={styles.button2} onPress={cloudinaryUpload}>
              {loading ? (
                <Spinner
                  animating={loading}
                  color="#000"
                  size={12}
                  style={{ marginVertical: -32 }}
                />
              ) : (
                <Text style={styles.text2}>OK</Text>
              )}
            </TouchableOpacity>
          )}
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 3,
    marginTop: getStatusBarHeight(),
    justifyContent: "center",
    alignItems: "center",
  },
  bottom: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "center",
    marginBottom: 56,
  },
  button: {
    backgroundColor: Colors.DARK_PRIMARY,
    paddingVertical: 16,
    paddingHorizontal: 36,
    borderRadius: 24,
    width: 180,
    marginTop: 48,
  },
  button2: {
    backgroundColor: Colors.WHITE,
    paddingVertical: 16,
    paddingHorizontal: 36,
    borderRadius: 24,
    width: 180,
  },
  text: {
    color: Colors.WHITE,
    fontWeight: "bold",
    textAlign: "center",
  },
  text2: {
    color: Colors.DARK_PRIMARY,
    fontWeight: "bold",
    textAlign: "center",
  },
  image: {
    width: 180,
    height: 180,
    borderRadius: 90,
  },
});
