import React, { useState, useEffect } from "react";
import { StyleSheet, View, FlatList, Platform, Alert } from "react-native";
import AppButton from "../components/AppButton";
import Colors from "../constants/Colors";
import { AppCard } from "../components/AppCard";
import { Fab, Icon, Button, Text, Spinner } from "native-base";
import { Ionicons } from "@expo/vector-icons";
import { Entypo } from "@expo/vector-icons";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { getOneTask, getSession, getAllTask, getTodayTask } from "../api";
import { useFocusEffect } from "@react-navigation/native";

const TaskList = (props) => {
  const { navigation } = props;
  const [fab, setfab] = useState(false);
  const [upcoming, setupcoming] = useState(null);
  const [today, settoday] = useState([]);
  const [loading, setloading] = useState(false);
  const [loadingAll, setloadingAll] = useState(false);
  const [refreshing, setrefreshing] = useState(false);
  const month = new Array();
  month[0] = "January";
  month[1] = "February";
  month[2] = "March";
  month[3] = "April";
  month[4] = "May";
  month[5] = "June";
  month[6] = "July";
  month[7] = "August";
  month[8] = "September";
  month[9] = "October";
  month[10] = "November";
  month[11] = "December";

  const week = new Array();
  week[0] = "SUN";
  week[1] = "MON";
  week[2] = "TUE";
  week[3] = "WED";
  week[4] = "THU";
  week[5] = "FRI";
  week[6] = "SAT";

  useEffect(() => {
    updateTask();
  }, [new Date(Date.now()).getMinutes()]);

  const updateTask = () => {
    setrefreshing(true);
    updateUpcoming();
    updateToday();
    setTimeout(() => {
      if (loading === false && loadingAll === false) {
        setrefreshing(false);
      } else {
        setTimeout(() => {
          setrefreshing(false);
        }, 3000);
      }
    }, 5000);
  };

  const updateUpcoming = () => {
    setupcoming(null);
    setloading(true);
    getSession()
      .then((sesres) => {
        getOneTask(sesres.data.ID)
          .then((res) => {
            console.log("res", res.data);
            setupcoming(res.data);
            setloading(false);
          })
          .catch((err) => {
            setloading(false);
            setrefreshing(false);
          });
      })
      .catch((err) => {
        alert(err);
        setrefreshing(false);
      });
  };

  const updateToday = () => {
    setloadingAll(true);
    getSession()
      .then((sesres) => {
        getTodayTask(sesres.data.ID)
          .then((res) => {
            console.log("res------------>", res.data);
            // var temp = [];
            // temp.push(res.data);
            settoday(res.data);
            setloadingAll(false);
            // setTimeout(() => {
            //   setrefreshing(false);
            // }, 5000);
          })
          .catch((err) => {
            setloadingAll(false);
            setrefreshing(false);
          });
      })
      .catch((err) => {
        alert(err);
        setrefreshing(false);
      });
  };

  console.log("today", today);
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.boldText}>{month[new Date().getMonth()]}</Text>

        <Text style={styles.quoteText}>
          {new Date().toLocaleTimeString([], {
            hour: "2-digit",
            minute: "2-digit",
          })}
        </Text>

        <View style={{ marginBottom: 24 }} />
        <View>
          {loading ? (
            <Spinner animating={loading} color="#000" size={12} />
          ) : upcoming ? (
            <AppCard
              primary
              name={upcoming.taskName}
              tag={upcoming.tag}
              date={new Date(upcoming.date).toLocaleTimeString([], {
                hour: "2-digit",
                minute: "2-digit",
              })}
            />
          ) : (
            <Text style={styles.quoteText}>You are free</Text>
            // <AppCard primary name="Watering" tag="Not Critical" date="12:00" />
          )}
        </View>
      </View>
      <View style={styles.listGroup}>
        {/* {task.map((item) => {
          return <AppCard name={item.name} tag={item.tag} date={item.date} />;
        })} */}

        <View style={{ flex: 1, flexDirection: "row" }}>
          {/* <View style={styles.leftList}> */}
          {/* <Text style={{ fontSize: 36, fontWeight: "bold" }}>15</Text>
                <Text note>WED</Text> */}
          {/* </View> */}
          <View
            style={styles.rightList}
            onTouchMove={(e) => {
              if (
                e.nativeEvent.changedTouches[0].locationY >= Platform.OS ==
                "android"
                  ? 800
                  : 0
              ) {
              } else {
                if (fab === true) {
                  setfab(false);
                }
              }
            }}
          >
            <FlatList
              data={today}
              refreshing={refreshing}
              onRefresh={updateTask}
              renderItem={({ item, index }) => (
                <AppCard
                  key={index}
                  name={item.taskName}
                  tag={item.tag}
                  date={new Date(item.date).toLocaleTimeString([], {
                    hour: "2-digit",
                    minute: "2-digit",
                  })}
                  shareButtn
                  cardStyle={{ marginLeft: 88 }}
                  primary
                />
              )}
              ListEmptyComponent={() => (
                <Text note style={{ textAlign: "center", marginTop: 24 }}>
                  NO TASK FOR TODAY!
                </Text>
              )}
              ListHeaderComponent={(data) => (
                <>
                  {today.length !== 0 ? (
                    <View>
                      <Text style={{ fontSize: 28, fontWeight: "bold" }}>
                        {new Date().getDate()}
                      </Text>
                      <Text note style={{ fontSize: 20 }}>
                        {week[new Date().getDay()]}
                      </Text>
                    </View>
                  ) : null}
                </>
              )}
              ListHeaderComponentStyle={{
                position: "absolute",
                left: 24,
                top: 12,
              }}
              keyExtractor={(item) => item.ID}
              // showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
            />
          </View>
        </View>
      </View>
      {/* -----------------------------------FAV------------------------------------ */}
      <Fab
        active={fab}
        direction="up"
        containerStyle={{}}
        style={{ backgroundColor: Colors.BLACK, zIndex: 999 }}
        position="bottomRight"
        onPress={() => setfab(!fab)}
        // onPress={() => navigation.navigate("Create")}
      >
        <Ionicons name="md-add" size={24} color={Colors.BLACK} />
        <Button
          style={{ backgroundColor: Colors.BLACK, zIndex: 999 }}
          onPress={() => navigation.navigate("Create")}
        >
          {/* <View
            style={{
              left: -158,
              position: "absolute",
              height: 40,
              width: 40,
              backgroundColor: Colors.BLACK,
              borderTopStartRadius: 20,
              borderBottomStartRadius: 20,
            }}
          />
          <Text
            style={{
              left: -140,
              position: "absolute",
              fontWeight: "bold",
              padding: 10,
              backgroundColor: Colors.BLACK,
              borderTopStartRadius: 50,
              borderBottomStartRadius: 50,
            }}
          >
            ADD NEW TASK
          </Text> */}

          <Entypo name="add-to-list" size={18} color="white" />
        </Button>
        {/* <Button style={{ backgroundColor: Colors.BLACK }}>
          <Text
            style={{
              left: -56,
              position: "absolute",
              fontWeight: "bold",
              padding: 10,
              backgroundColor: Colors.BLACK,
              borderTopLeftRadius: 50,
              borderBottomStartRadius: 50,
            }}
          >
            TEST
          </Text>
          <Entypo name="add-to-list" size={18} color="white" />
        </Button> */}
      </Fab>
    </View>
  );
};

export default TaskList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
    height: "100%",
    alignItems: "center",
    paddingTop: Platform.OS === "android" ? 0 : 0,
  },
  header: {
    flex: Platform.OS === "android" ? 2 : 1,
    justifyContent: "center",
    backgroundColor: Colors.PRIMARY,
    width: "100%",
    paddingHorizontal: 24,
    paddingVertical: 24,
  },
  listGroup: {
    flex: 3,
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  boldText: {
    color: Colors.WHITE,
    fontWeight: "bold",
    fontSize: 24,
  },
  quoteText: {
    color: Colors.WHITE,
    opacity: 0.5,
    fontSize: 20,
  },
  leftList: {
    flex: 1,
    alignItems: "center",
    marginTop: 12,
    zIndex: -999,
    // backgroundColor: Colors.PRIMARY,
    // paddingHorizontal: 12,
  },
  rightList: {
    flex: 5,
    paddingRight: 24,
    zIndex: 999,
    // backgroundColors: Colors.DARK_PRIMARY,
  },
});
