import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  Alert,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar,
} from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import { Spinner, Input, Item, Col } from "native-base";
import Colors from "../constants/Colors";
import { signin } from "../api";
import { AsyncStorage } from "react-native";
import { useDispatch } from "react-redux";
import { loginSuccess } from "../redux/actions/authAction";

export const Login = (props) => {
  const { navigation } = props;
  const dispatch = useDispatch();
  const [email, setemail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setloading] = useState(false);
  const token = AsyncStorage.getItem("token");
  console.log("token", token);
  const loginHandle = () => {
    setloading(true);

    signin(email, password).then((res) => {
      console.log("res", res.data);
      if (res) {
        AsyncStorage.setItem("token", res.data.token);
        dispatch(loginSuccess(res.data.token));
        setTimeout(() => {
          setloading(false);
          navigation.navigate("SecondPart");
        }, 1000);
      }
      if (!res) {
        setloading(false);
        alert("Can not login");
      }
    });
  };

  return (
    <>
      <StatusBar
        translucent
        backgroundColor="transparent"
        barStyle="light-content"
      />
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        style={{ backgroundColor: Colors.DARK_PRIMARY }}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: Colors.DARK_PRIMARY,
          }}
        >
          <View style={styles.row1}>
            <Image
              resizeMode="stretch"
              style={styles.image}
              source={require("../assets/images/logo.png")}
            />
            <Text
              style={{
                color: Colors.WHITE,
                fontSize: 24,
                fontWeight: "bold",
                marginBottom: 24,
              }}
            >
              TA2K
            </Text>
          </View>
          <View style={styles.row2}>
            <KeyboardAwareScrollView>
              <Item rounded style={styles.user}>
                <Input
                  style={{ color: Colors.DARK_PRIMARY, textAlign: "center" }}
                  round
                  onChangeText={(text) => setemail(text)}
                  placeholder="E-mail"
                  placeholderTextColor={Colors.PRIMARY}
                  value={email}
                />
              </Item>
              <Item rounded style={styles.pass}>
                <Input
                  style={{ color: Colors.DARK_PRIMARY, textAlign: "center" }}
                  onChangeText={(text) => setPassword(text)}
                  secureTextEntry={true}
                  placeholder="Password"
                  placeholderTextColor={Colors.PRIMARY}
                  value={password}
                />
              </Item>

              <TouchableOpacity onPress={loginHandle}>
                <View style={styles.row2_item}>
                  {loading === false ? (
                    <Text style={styles.textBox}>Login</Text>
                  ) : (
                    <Spinner
                      animating={loading}
                      color="#000"
                      size={12}
                      style={{ marginTop: -20 }}
                    />
                  )}
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => Alert.alert("So sorry for your lost")}
              >
                <Text style={{ color: Colors.BLACK, textAlign: "center" }}>
                  Forgot Password?
                </Text>
              </TouchableOpacity>
            </KeyboardAwareScrollView>
          </View>
          <View style={styles.row3}>
            <View style={styles.row3_item2}>
              <TouchableOpacity onPress={() => navigation.navigate("Register")}>
                <Text style={{ color: Colors.BLACK, textAlign: "right" }}>
                  Don't have account yet? Sign up!
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </>
  );
};

export default Login;

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    backgroundColor: "#111",
    // paddingHorizontal: 10,
  },
  textBox: {
    alignSelf: "center",
    color: "#000",
    marginVertical: 10,
    fontSize: 15,
    fontWeight: "bold",
  },
  image: {
    // borderRadius: 100,
    // overflow: 'hidden',
    width: 120,
    height: 120,
    // borderWidth: 5,
    // borderColor: 'white',
  },
  row1: {
    flex: 6,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: 10,
  },
  row2: {
    flex: 6,
    flexDirection: "column",
    alignContent: "center",
    justifyContent: "center",
    marginHorizontal: 40,
    marginVertical: 20,
  },
  row3: {
    flex: 1,
    marginBottom: 20,
    justifyContent: "flex-end",
  },
  row2_item: {
    width: "100%",
    height: 40,
    backgroundColor: Colors.WHITE,
    borderTopStartRadius: 30,
    borderTopEndRadius: 30,
    borderBottomStartRadius: 30,
    borderBottomEndRadius: 30,
    elevation: 5,
    marginBottom: 15,
  },
  user: {
    backgroundColor: Colors.SECONDARY,
  },
  pass: {
    backgroundColor: Colors.SECONDARY,
    marginVertical: 20,
  },
  row3_item1: {
    flexDirection: "row",
    alignContent: "flex-start",
    justifyContent: "center",
  },
  row3_item2: {
    flexDirection: "row",
    alignContent: "flex-end",
    justifyContent: "center",
  },
});
