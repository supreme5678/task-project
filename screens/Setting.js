import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Button,
  ScrollView,
  Platform,
  Switch,
} from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { SimpleLineIcons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { AsyncStorage } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../redux/actions/authAction";
import AppButton from "../components/AppButton";
import { toggleChat } from "../redux/actions/settingAction";
const Setting = (props) => {
  const { navigation } = props;
  const dispatch = useDispatch();
  const [loading, setloading] = useState(false);
  const [isEnabled, setIsEnabled] = useState(false);

  const chat = useSelector((state) => state.settingReducer.chat);
  console.log("chat", chat);

  const toggleSwitch = () => {
    setIsEnabled((previousState) => !previousState);
    dispatch(toggleChat());
  };

  const onLogout = () => {
    setloading(true);
    AsyncStorage.removeItem("token").then(() => {});
    setTimeout(() => {
      setloading(false);
      dispatch(logout());
    }, 3000);
  };
  return (
    <>
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        style={{ backgroundColor: Colors.PRIMARY }}
      >
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.headerTxt}>Setting</Text>
          </View>
          <View style={styles.content}>
            <View style={styles.content1}>
              <Text style={{ fontWeight: "bold", paddingTop: 8 }}>CHAT</Text>
              <Switch
                trackColor={{ false: "#767577", true: Colors.SECONDARY }}
                thumbColor={isEnabled ? Colors.PRIMARY : Colors.WHITE}
                ios_backgroundColor={Colors.SECONDARY}
                onValueChange={toggleSwitch}
                value={isEnabled}
              />
            </View>
          </View>
          <View style={styles.bottom}>
            <AppButton title="LOGOUT" onPress={onLogout} loading={loading} />
          </View>
        </View>
      </ScrollView>
    </>
  );
};

export default Setting;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: Platform.OS === "android" ? getStatusBarHeight() : 0,
  },
  header: {
    flex: Platform.OS === "android" ? 1 : 1,
    justifyContent: "center",
    backgroundColor: Colors.PRIMARY,
    width: "100%",
    paddingHorizontal: 24,
    paddingVertical: Platform.OS === "android" ? 48 : 56,
  },
  content: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: Colors.WHITE,
    width: "100%",
    paddingHorizontal: 24,
    paddingVertical: 24,
  },
  content1: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bottom: {
    flex: 8,
    backgroundColor: Colors.WHITE,
    justifyContent: "flex-end",
    alignItems: "center",
    paddingBottom: 56,
  },
  headerTxt: {
    fontSize: 24,
    fontWeight: "bold",
    marginTop: Platform.OS === "ios" ? 24 : 0,
  },
});
