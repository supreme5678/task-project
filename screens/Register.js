import React, { useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  Alert,
  ScrollView,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar,
  Platform,
  AsyncStorage,
} from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";
import Colors from "../constants/Colors";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import { Spinner, Input, Item, Thumbnail } from "native-base";
import AppButton from "../components/AppButton";
import { signup } from "../api";

export default function Register(props) {
  const { navigation } = props;
  const [email, setemail] = useState("");
  const [password, setPassword] = useState("");
  const [firstname, setfirstname] = useState("");
  const [lastname, setlastname] = useState("");
  const [loading, setloading] = useState(false);

  const handleSignup = () => {
    // navigation.navigate("RegisterImg");
    setloading(true);
    const profile_image =
      "https://kyau.edu.bd/kyau/public/uploads/defult_image.png";
    signup({ firstname, lastname, email, password, profile_image })
      .then((res) => {
        AsyncStorage.setItem("token", res.data.token).then(() => {
          setTimeout(() => {
            setloading(false);
            Platform.OS === "ios"
              ? navigation.navigate("RegisterImg")
              : navigation.navigate("Welcome");
          }, 3000);
        });
      })
      .catch((err) => {
        console.log("err", err);
        setloading(false);
      });
  };
  return (
    <>
      {Platform.OS === "ios" && (
        <StatusBar
          translucent
          backgroundColor="transparent"
          barStyle="light-content"
        />
      )}
      <ScrollView
        contentContainerStyle={{ flexGrow: 1 }}
        style={{ backgroundColor: Colors.SECONDARY }}
      >
        <View style={styles.container}>
          <Text
            style={{
              color: Colors.WHITE,
              textAlign: "center",
              fontSize: 18,
              fontWeight: "bold",
              marginVertical: 24,
            }}
          >
            Register
          </Text>
          <KeyboardAwareScrollView>
            <Item rounded style={styles.pass}>
              <Input
                round
                onChangeText={(evt) => setemail(evt)}
                placeholder="E-mail"
                placeholderTextColor={Colors.BLACK}
                value={email}
                style={{ color: Colors.WHITE, textAlign: "center" }}
              />
            </Item>
            <Item rounded style={styles.pass}>
              <Input
                onChangeText={(text) => setPassword(text)}
                secureTextEntry={true}
                placeholder="Password"
                placeholderTextColor={Colors.BLACK}
                value={password}
                style={{ color: Colors.WHITE, textAlign: "center" }}
              />
            </Item>
            <Item rounded style={styles.pass}>
              <Input
                round
                onChangeText={(text) => setfirstname(text)}
                placeholder="First name"
                placeholderTextColor={Colors.BLACK}
                value={firstname}
                style={{ color: Colors.WHITE, textAlign: "center" }}
              />
            </Item>
            <Item rounded style={styles.pass}>
              <Input
                onChangeText={(text) => setlastname(text)}
                placeholder="Last name"
                placeholderTextColor={Colors.BLACK}
                value={lastname}
                style={{ color: Colors.WHITE, textAlign: "center" }}
              />
            </Item>
          </KeyboardAwareScrollView>
          <TouchableOpacity onPress={handleSignup}>
            <View style={styles.row2_item}>
              {loading === false ? (
                <Text style={styles.textBox}>Register</Text>
              ) : (
                <Spinner
                  animating={loading}
                  color="#000"
                  size={12}
                  style={{ marginTop: -20 }}
                />
              )}
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: getStatusBarHeight(),
    paddingHorizontal: 36,
    flex: 1,
  },
  user: {
    backgroundColor: Colors.DARK_PRIMARY,
  },
  pass: {
    backgroundColor: Colors.DARK_PRIMARY,
    marginVertical: 20,
  },
  row2_item: {
    width: "100%",
    height: 40,
    backgroundColor: Colors.WHITE,
    borderTopStartRadius: 30,
    borderTopEndRadius: 30,
    borderBottomStartRadius: 30,
    borderBottomEndRadius: 30,
    elevation: 5,
    marginBottom: 64,
  },
  textBox: {
    alignSelf: "center",
    color: "#000",
    marginVertical: 10,
    fontSize: 15,
    fontWeight: "bold",
  },
  imageUser: {
    flex: 1,
    alignItems: "center",
    marginVertical: 24,
  },
});
