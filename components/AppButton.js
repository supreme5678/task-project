import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import Colors from "../constants/Colors";
import { Spinner } from "native-base";

const AppButton = ({ title, loading, ...props }) => {
  return (
    <View>
      <TouchableOpacity {...props} style={styles.button}>
        {loading ? (
          <Spinner
            color={Colors.DARK_PRIMARY}
            size={24}
            style={{ marginVertical: -32 }}
          />
        ) : (
          <Text style={styles.text}>{title}</Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

export default AppButton;

const styles = StyleSheet.create({
  button: {
    backgroundColor: Colors.SECONDARY,
    paddingVertical: 16,
    paddingHorizontal: 36,
    borderRadius: 24,
    width: 180,
  },
  text: {
    color: Colors.WHITE,
    fontWeight: "bold",
    textAlign: "center",
  },
});
