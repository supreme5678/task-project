import React from "react";
import { StyleSheet, View, Image, TouchableOpacity, Share } from "react-native";
import {
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  Icon,
  Left,
  Body,
  Right,
  Item,
  SwipeRow,
} from "native-base";
import Colors from "../constants/Colors";
import { Entypo } from "@expo/vector-icons";

export const AppCard = ({
  name,
  tag,
  date,
  primary,
  shareButtn,
  cardStyle,
  keyColor,
  onSharePress,
}) => {
  console.log("tag--------->", tag);
  const handleTag = (tag) => {
    switch (tag) {
      case "Important":
        return "red";
      case "In progress":
        return "orange";
      case "Deadline":
        return "yellow";
      case "Work":
        return "purple";
      case "Family":
        return "lightgreen";
      case "Not Critical":
        return "lightblue";
      default:
        return "white";
    }
  };

  const onShare = async () => {
    try {
      const result = await Share.share({
        url: "www.google.com",
        title: "TA2k",
        message: "At " + date + " I will " + name,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          alert("shared");
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  return (
    <Card style={[styles.card, cardStyle]}>
      <CardItem
        style={{
          borderBottomWidth: 1,
          borderBottomColor: Colors.BLACK,
          // backgroundColor: "#fff",
          backgroundColor: primary ? Colors.DARK_PRIMARY : Colors.PRIMARY,
          borderTopLeftRadius: 12,
          borderTopRightRadius: 12,
        }}
      >
        <Left>
          {/* <Thumbnail source={{ uri: "Image URL" }} /> */}

          <Body>
            <Text style={{ color: Colors.WHITE, fontWeight: "bold" }}>
              {name}
            </Text>
            <Text note style={styles.note}>
              <Thumbnail
                style={{
                  height: 10,
                  width: 24,
                  backgroundColor: handleTag(tag),
                  borderColor: Colors.WHITE,
                  borderWidth: 1,
                }}
              />
              {tag}
            </Text>
          </Body>
        </Left>
      </CardItem>
      <CardItem style={styles.bottom}>
        <Left>
          <Text style={styles.boldTxt}>{date}</Text>
        </Left>
        {shareButtn ? (
          <Right>
            <TouchableOpacity onPress={onShare}>
              <Entypo name="share" size={16} color="black" />
            </TouchableOpacity>
          </Right>
        ) : null}
      </CardItem>
    </Card>
  );
};

const styles = StyleSheet.create({
  card: {
    borderRadius: 12,
  },
  //   top:
  bottom: {
    marginVertical: 8,
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12,
    backgroundColor: "rgba(255,255,255,0.2)",
  },

  note: {
    color: "#ddd",
  },
  boldTxt: {
    fontSize: 16,
    fontWeight: "bold",
  },
});
