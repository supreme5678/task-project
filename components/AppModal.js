import React, { useState } from "react";
import { Button, Text, View, StyleSheet } from "react-native";
import Modal from "react-native-modal";
import Colors from "../constants/Colors";
export function AppModal({ isVisible, item }) {
  const [isModalVisible, setModalVisible] = useState(isVisible);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  return (
    <Modal
      isVisible={isVisible}
      backdropOpacity={0.5}
      animationIn={"zoomInDown"}
      animationOut={"zoomOutUp"}
      backdropTransitionInTiming={1000}
      backdropTransitionOutTiming={1000}
    >
      <View
        style={{
          display: "flex",
          backgroundColor: Colors.SECONDARY,
          paddingVertical: 24,
          borderRadius: 5,
          height: 240,
        }}
      >
        <Text>Wow</Text>
        <Button title="Hide modal" onPress={toggleModal} />
      </View>
    </Modal>
  );
}

const styles = StyleSheet.create({});
