import { LOGIN, LOGOUT } from "../actions/authAction";

const AUTH_STATE = {
  isLogin: false,
  token: undefined,
  error: "",
};

export const authReducer = (state = AUTH_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        token: action.payLoad,
        isLogin: true,
      };
    case LOGOUT:
      return AUTH_STATE;
    default:
      return state;
  }
};
