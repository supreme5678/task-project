import { SETTING_CHAT } from "../actions/settingAction";

const SETTING_STATE = {
  chat: false,
};

export const settingReducer = (state = SETTING_STATE, action) => {
  switch (action.type) {
    case SETTING_CHAT:
      return { chat: !state.chat };
    default:
      return state;
  }
};
