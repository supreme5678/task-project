import { combineReducers } from "redux";
import { authReducer } from "./reducers/authReducer";
import { settingReducer } from "./reducers/settingReducer";

export const rootReducer = combineReducers({
  authReducer,
  settingReducer,
});
