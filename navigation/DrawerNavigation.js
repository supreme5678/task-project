import * as React from "react";
import { Button, View } from "react-native";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import TaskList from "../screens/TaskList";
import Calendar from "../screens/Calendar";
import FriendList from "../screens/FriendList";
import Profile from "../screens/Profile";
import Setting from "../screens/Setting";

const Drawer = createDrawerNavigator();

export default function DrawerNavigation() {
  return <NavigationContainer></NavigationContainer>;
}
