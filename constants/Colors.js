const tintColor = "#2f95dc";

export default {
  tintColor,
  tabIconDefault: "#ccc",
  tabIconSelected: tintColor,
  tabBar: "#fefefe",
  errorBackground: "red",
  errorText: "#fff",
  warningBackground: "#EAEB5E",
  warningText: "#666804",
  noticeBackground: tintColor,
  noticeText: "#fff",
  PRIMARY: "rgba(237,128,116,0.8)",
  DARK_PRIMARY: "rgba(237,128,116,1)",
  SECONDARY: "#141414",
  WHITE: "#FFF",
  BLACK: "#000",
  TRANSPARENT: "transparent",
};
