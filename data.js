export const task = [
  {
    id: 1,
    taskName: "Do homework",
    tag: "Imediately",
    startDate: "2020-06-07T07:51:26.077Z",
    endDate: "2020-06-07T07:52:07.483Z",
    location: {
      lat: 1,
      long: 2,
    },
    note: "Math and English",
  },
  {
    id: 2,
    taskName: "Watering",
    tag: "Later",
    startDate: "2020-06-07T07:51:26.077Z",
    endDate: "2020-06-07T07:52:07.483Z",
    location: {
      lat: 1,
      long: 2,
    },
    note: "oak trees",
  },
  {
    id: 3,
    taskName: "Workout",
    tag: "Imediately",
    startDate: "2020-06-07T07:51:26.077Z",
    endDate: "2020-06-07T07:52:07.483Z",
    location: {
      lat: 1,
      long: 2,
    },
    note: "running",
  },
  {
    id: 4,
    taskName: "Project",
    tag: "Imediately",
    startDate: "2020-06-07T07:51:26.077Z",
    endDate: "2020-06-07T07:52:07.483Z",
    location: {
      lat: 1,
      long: 2,
    },
    note: "dv app",
  },
];
